/* MIT License
 *
 * Copyright (c) [2020] [Zoltan Dolensky]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef CAN_ROUTER_TYPES_H_
#define CAN_ROUTER_TYPES_H_

#include "can_pal.h"

/*
 * ! @file can_router_types.h
 */

/*
 * ! @brief CAN router available channel indexes
 */
typedef enum {
    CAN_ROUTER_CHANNEL0,        /*!< Channel 0 (FlexCAN0) */
    CAN_ROUTER_CHANNEL1,        /*!< Channel 1 (FlexCAN1) */
    CAN_ROUTER_CHANNEL2,        /*!< Channel 2 (FlexCAN2) */
    CAN_ROUTER_CHANNEL_COUNT    /*!< Maximum channel count */
} can_router_channel_t;

/*
 * ! @brief CAN router mailbox aquisition types
 */
typedef enum {
    CAN_ROUTER_MB_FREE,         /*!< Mailbox is unused and available for aquisition */
    CAN_ROUTER_MB_RX,           /*!< Mailbox is aquired for message reception */
    CAN_ROUTER_MB_TX,           /*!< Mailbox is aquired for message transmission */
    CAN_ROUTER_MB_RTR_RES       /*!< Mailbox is aquired for responding to remote frames */
} can_router_aquire_type_t;

/*
 * ! @brief Callback for asynchronous notifications from can_router
 */
typedef void (*can_router_callback_t)(can_router_channel_t channel, uint8_t mailbox, can_event_t event, void *context);

#endif /* CAN_ROUTER_TYPES_H_ */
