/* MIT License
 *
 * Copyright (c) [2020] [Zoltan Dolensky]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "can_router.h"
#include "can_router_config.h"

/*
 * ! @file can_router.c
 */

/*
 * --------------------------------------------------------------------------------------------------------------------
 */

/*
 * ! @brief CAN router internal states
 */
typedef enum {
    CAN_ROUTER_CHANNEL_STATE_NONE,      /*!< Instance is not created */
    CAN_ROUTER_CHANNEL_STATE_INIT,      /*!< Instance created, channel closed */
    CAN_ROUTER_CHANNEL_STATE_IDLE,      /*!< Channel opened */
    CAN_ROUTER_CHANNEL_STATE_BUSY       /*!< At least one of the mailboxes has aquired lock */
} can_router_channel_state_t;

/*
 * ! @brief Internal driver structure
 */
typedef struct {
    uint8_t aquired[NO_OF_FLEXCAN_INSTS_FOR_CAN];                       /*!< Counter for aquired locks */
    can_router_channel_state_t states[NO_OF_FLEXCAN_INSTS_FOR_CAN];     /*!< Channel states */
    can_router_channel_handle_t channels[NO_OF_FLEXCAN_INSTS_FOR_CAN];  /*!< Channel handles */
} can_router_driver_t;

/*
 * ! @brief Internal driver instance
 */
static can_router_driver_t _can_router_driver = { 0 };

/*
 * Internal event handler
 * --------------------------------------------------------------------------------------------------------------------
 */
static void _can_router_event_handler(uint32_t channel, can_event_t event, uint32_t mailbox, void *context) {
    (void)context;
    can_router_channel_handle_t *handle = &_can_router_driver.channels[channel];
    can_router_callback_t callback = handle->mboxes[channel].callback;
    void * ctx = handle->mboxes[channel].context;
    if (callback != NULL) {
        callback(channel, mailbox, event, ctx);
    }
}

/*
 * --------------------------------------------------------------------------------------------------------------------
 */
status_t can_router_channel_create(can_router_channel_t channel) {
    if (channel > CAN_ROUTER_CHANNEL_COUNT) {
        return STATUS_UNSUPPORTED;
    }
    if(_can_router_driver.states[channel] != CAN_ROUTER_CHANNEL_STATE_NONE){
        return STATUS_ERROR;
    }
    can_router_channel_handle_t *handle = &_can_router_driver.channels[channel];
    handle->inst.instType = CAN_INST_TYPE_FLEXCAN;
    handle->inst.instIdx = channel;
    handle->cancfg = &can_channel_init_cfg;
    handle->clkname = FlexCAN0_CLK + channel;
    handle->pins = &can_pins_init_cfg[channel];
    for (uint8_t i = 0; i < 16; i++) {
        handle->mboxes[i] = (can_router_mailbox_handle_t ) { 0 };
    }
    _can_router_driver.aquired[channel] = 0;
    _can_router_driver.states[channel] = CAN_ROUTER_CHANNEL_STATE_INIT;
    return STATUS_SUCCESS;
}

/*
 * --------------------------------------------------------------------------------------------------------------------
 */
status_t can_router_channel_destroy(can_router_channel_t channel) {
    if (channel > CAN_ROUTER_CHANNEL_COUNT) {
        return STATUS_UNSUPPORTED;
    }
    if (_can_router_driver.states[channel] != CAN_ROUTER_CHANNEL_STATE_INIT) {
        return STATUS_BUSY;
    }
    _can_router_driver.channels[channel] = (can_router_channel_handle_t ) { 0 };
    _can_router_driver.aquired[channel] = 0;
    _can_router_driver.states[channel] = CAN_ROUTER_CHANNEL_STATE_NONE;
    return STATUS_SUCCESS;
}

/*
 * --------------------------------------------------------------------------------------------------------------------
 */
status_t can_router_channel_open(can_router_channel_t channel) {
    if (channel > CAN_ROUTER_CHANNEL_COUNT) {
        return STATUS_UNSUPPORTED;
    }
    if (_can_router_driver.states[channel] != CAN_ROUTER_CHANNEL_STATE_INIT) {
        return STATUS_ERROR;
    }
    status_t result = STATUS_ERROR;
    can_router_channel_handle_t *handle = &_can_router_driver.channels[channel];
    CLOCK_DRV_SetModuleClock(handle->clkname, NULL);
    CLOCK_DRV_SetModuleClock(handle->pins->rxd.port, NULL);
    CLOCK_DRV_SetModuleClock(handle->pins->txd.port, NULL);
    if (handle->pins != NULL) {
        PINS_DRV_Init(1, &handle->pins->rxd.pincfg);
        PINS_DRV_Init(1, &handle->pins->txd.pincfg);
    }
    result = CAN_Init(&handle->inst, handle->cancfg);
    if (result == STATUS_SUCCESS) {
        CAN_InstallEventCallback(&handle->inst, _can_router_event_handler, handle);
        _can_router_driver.aquired[channel] = 0;
        _can_router_driver.states[channel] = CAN_ROUTER_CHANNEL_STATE_IDLE;
    }
    return result;
}

/*
 * --------------------------------------------------------------------------------------------------------------------
 */
status_t can_router_channel_close(can_router_channel_t channel) {
    if (channel > CAN_ROUTER_CHANNEL_COUNT) {
        return STATUS_UNSUPPORTED;
    }
    if (_can_router_driver.states[channel] != CAN_ROUTER_CHANNEL_STATE_IDLE) {
        return STATUS_ERROR;
    }
    can_router_channel_handle_t *handle = &_can_router_driver.channels[channel];
    CLOCK_DRV_SetModuleClock(handle->clkname, NULL);
    if (handle->pins != NULL) {
        PINS_DRV_SetMuxModeSel(handle->pins->rxd.pincfg.base, handle->pins->rxd.pincfg.pinPortIdx, PORT_PIN_DISABLED);
        PINS_DRV_SetMuxModeSel(handle->pins->txd.pincfg.base, handle->pins->txd.pincfg.pinPortIdx, PORT_PIN_DISABLED);
    }
    CAN_Deinit(&handle->inst);
    /* NXP pls, provide SDK function for disabling peripherals */
    PCC->PCCn[handle->clkname] &= ~PCC_PCCn_CGC_MASK;
    _can_router_driver.states[channel] = CAN_ROUTER_CHANNEL_STATE_INIT;
    return STATUS_SUCCESS;
}

/*
 * --------------------------------------------------------------------------------------------------------------------
 */
status_t can_router_get_free_mailbox(can_router_channel_t channel, uint8_t *mailbox) {
    if (channel > CAN_ROUTER_CHANNEL_COUNT) {
        return STATUS_UNSUPPORTED;
    }
    if (_can_router_driver.states[channel] < CAN_ROUTER_CHANNEL_STATE_IDLE) {
        return STATUS_ERROR;
    }
    can_router_channel_handle_t *handle = &_can_router_driver.channels[channel];
    for (uint8_t i = 0; i < 16; i++) {
        if (!(handle->mboxes[i].aquired)) {
            *mailbox = i;
            return STATUS_SUCCESS;
        }
    }
    return STATUS_BUSY;
}

/*
 * --------------------------------------------------------------------------------------------------------------------
 */
status_t can_router_aquire_mailbox(can_router_channel_t channel, uint8_t mailbox, can_router_callback_t callback,
        void *context) {
    if (channel > CAN_ROUTER_CHANNEL_COUNT || mailbox >= 16) {
        return STATUS_UNSUPPORTED;
    }
    if (_can_router_driver.states[channel] < CAN_ROUTER_CHANNEL_STATE_IDLE) {
        return STATUS_ERROR;
    }
    can_router_channel_handle_t *handle = &_can_router_driver.channels[channel];
    if (handle->mboxes[mailbox].aquired) {
        return STATUS_ERROR;
    }
    handle->mboxes[mailbox].aquired = true;
    handle->mboxes[mailbox].callback = callback;
    handle->mboxes[mailbox].context = context;
    _can_router_driver.states[channel] = CAN_ROUTER_CHANNEL_STATE_BUSY;
    _can_router_driver.aquired[channel]++;
    return STATUS_SUCCESS;
}

status_t can_router_free_mailbox(can_router_channel_t channel, uint8_t mailbox) {
    if (channel > CAN_ROUTER_CHANNEL_COUNT || mailbox >= 16) {
        return STATUS_UNSUPPORTED;
    }
    if (_can_router_driver.states[channel] < CAN_ROUTER_CHANNEL_STATE_IDLE) {
        return STATUS_ERROR;
    }
    can_router_channel_handle_t *handle = &_can_router_driver.channels[channel];
    if (!(handle->mboxes[mailbox].aquired)) {
        /* Mailbox is already free */
        return STATUS_SUCCESS;
    }
    status_t mbstate = STATUS_BUSY;
    /* TODO: Abort transfer and free (could fill with remote frame responses) */
    mbstate = CAN_GetTransferStatus(&handle->inst, mailbox);
    if (mbstate == STATUS_SUCCESS) {
        handle->mboxes[mailbox] = (can_router_mailbox_handle_t ) { 0 };
        _can_router_driver.aquired[channel]--;
        if (_can_router_driver.aquired == 0) {
            _can_router_driver.states[channel] = CAN_ROUTER_CHANNEL_STATE_IDLE;
        }
    }
    return mbstate;
}

/*
 * --------------------------------------------------------------------------------------------------------------------
 */
status_t can_router_transmit(can_router_channel_t channel, uint8_t mailbox, can_message_t *message) {
    if (channel > CAN_ROUTER_CHANNEL_COUNT || mailbox >= 16) {
        return STATUS_UNSUPPORTED;
    }
    if (_can_router_driver.states[channel] < CAN_ROUTER_CHANNEL_STATE_IDLE) {
        return STATUS_ERROR;
    }
    status_t result = STATUS_ERROR;
    can_router_channel_handle_t *handle = &_can_router_driver.channels[channel];
    result = CAN_GetTransferStatus(&handle->inst, mailbox);
    if (result == STATUS_SUCCESS) {
        handle->mboxes[mailbox].type = CAN_ROUTER_MB_TX;
        handle->mboxes[mailbox].message = message;
        can_buff_config_t config = { 0 };
        message->id &= 0x1fffffff;
        if (message->id > 0x7ff) {
            config.idType = CAN_MSG_ID_EXT;
        }
        result = CAN_ConfigTxBuff(&handle->inst, mailbox, &config);
        if (result == STATUS_SUCCESS) {
            return CAN_Send(&handle->inst, mailbox, handle->mboxes[mailbox].message);
        }
    }
    return result;
}

/*
 * --------------------------------------------------------------------------------------------------------------------
 */
status_t can_router_set_rtr(can_router_channel_t channel, uint8_t mailbox, can_message_t *message) {
    return STATUS_UNSUPPORTED;
}

/*
 * --------------------------------------------------------------------------------------------------------------------
 */
status_t can_router_receive(can_router_channel_t channel, uint8_t mailbox, can_message_t *message) {
    if (channel > CAN_ROUTER_CHANNEL_COUNT || mailbox >= 16) {
        return STATUS_UNSUPPORTED;
    }
    if (_can_router_driver.states[channel] < CAN_ROUTER_CHANNEL_STATE_IDLE) {
        return STATUS_ERROR;
    }
    status_t result = STATUS_ERROR;
    can_router_channel_handle_t *handle = &_can_router_driver.channels[channel];
    result = CAN_GetTransferStatus(&handle->inst, mailbox);
    if (result == STATUS_SUCCESS) {
        handle->mboxes[mailbox].type = CAN_ROUTER_MB_RX;
        handle->mboxes[mailbox].message = message;
        can_buff_config_t config = { 0 };
        message->id &= 0x1fffffff;
        if (message->id > 0x7ff) {
            config.idType = CAN_MSG_ID_EXT;
        }
        result = CAN_ConfigRxBuff(&handle->inst, mailbox, &config, message->id);
        if (result == STATUS_SUCCESS) {
            return CAN_Receive(&handle->inst, mailbox, handle->mboxes[mailbox].message);
        }
    }
    return result;
}

/*
 * End of file
 * --------------------------------------------------------------------------------------------------------------------
 */

