/* MIT License
 *
 * Copyright (c) [2020] [Zoltan Dolensky]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef CAN_ROUTER_CONFIG_H_
#define CAN_ROUTER_CONFIG_H_

#include "can_router_types.h"

#include "pins_driver.h"
#include "clock_manager.h"

/*
 * ! @file can_router_config.h
 */

/*
 * ! @brief CAN router mailbox handle
 */
typedef struct {
    bool aquired;                           /*!< Mailbox aquisition lock */
    can_router_aquire_type_t type;          /*!< Type of mailbox usage */
    can_message_t *message;                 /*!< Pointer to message structure */
    can_router_callback_t callback;         /*!< Registered notification callback */
    void *context;                          /*!< User defined variable */
} can_router_mailbox_handle_t;

/*
 * ! @brief Single pin configuration structure
 */
typedef struct {
    clock_names_t port;                     /*!< Port of configured pin */
    const pin_settings_config_t pincfg;     /*!< Configuration structure of pin */
} can_router_pin_config;

/*
 * ! @brief Configuration structure for all used pins
 */
typedef struct {
    can_router_pin_config rxd;              /*!< Configuration values for CAN RXD pin */
    can_router_pin_config txd;              /*!< Configuration values for CAN TXD pin */
} can_router_pins_t;

/*
 * ! @brief Handle for single channel
 */
typedef struct {
    can_instance_t inst;                    /*!< Instance handle for can_pal library */
    const can_user_config_t *cancfg;        /*!< FlexCAN peripheral configuration structure */
    clock_names_t clkname;                  /*!< Allows enabling and disabling FlexCAN clock */
    const can_router_pins_t *pins;          /*!< Pointer to pin configuration assigned to channel */
    can_router_mailbox_handle_t mboxes[16]; /*!< Router mailbox pool */
} can_router_channel_handle_t;

/*
 * ! @brief Initialization structure template for CAN channel
 */
extern const can_user_config_t can_channel_init_cfg;

/*
 * ! @brief Initialization structure template for CAN pins
 */
extern const can_router_pins_t can_pins_init_cfg[3];

#endif /* CAN_ROUTER_CONFIG_H_ */
