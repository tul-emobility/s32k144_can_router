/* MIT License
 *
 * Copyright (c) [2020] [Zoltan Dolensky]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "can_router_config.h"

/*
 * ! @file can_router_config.c
 */

/*
 * --------------------------------------------------------------------------------------------------------------------
 */

/*
 * Default template for can_pal configuration
 */
const can_user_config_t can_channel_init_cfg = {
    .maxBuffNum = 16UL,
    .mode = CAN_NORMAL_MODE,
    .peClkSrc = CAN_CLK_SOURCE_PERIPH,
    .enableFD = false,
    .payloadSize = CAN_PAYLOAD_SIZE_8,
    .nominalBitrate = {
        .propSeg = 7,
        .phaseSeg1 = 4,
        .phaseSeg2 = 1,
        .preDivider = 5,
        .rJumpwidth = 1
    },
    .extension = NULL,
};

/*
 * --------------------------------------------------------------------------------------------------------------------
 */


/*
 * Default template for can_router pin configuration
 */
const can_router_pins_t can_pins_init_cfg[3] = {
    {

    .rxd = {
        .port = PORTC_CLK,
        .pincfg = {
            .base = PORTC,
            .pinPortIdx = 2u,
            .pullConfig = PORT_INTERNAL_PULL_NOT_ENABLED,
            .passiveFilter = false,
            .driveSelect = PORT_LOW_DRIVE_STRENGTH,
            .mux = PORT_MUX_ALT3,
            .pinLock = false,
            .intConfig = PORT_DMA_INT_DISABLED,
            .clearIntFlag = false,
            .gpioBase = NULL,
            .digitalFilter = false,
        }
    },
        .txd = {
            .port = PORTC_CLK,
            .pincfg = {
                .base = PORTC,
                .pinPortIdx = 3u,
                .pullConfig = PORT_INTERNAL_PULL_NOT_ENABLED,
                .passiveFilter = false,
                .driveSelect = PORT_LOW_DRIVE_STRENGTH,
                .mux = PORT_MUX_ALT3,
                .pinLock = false,
                .intConfig = PORT_DMA_INT_DISABLED,
                .clearIntFlag = false,
                .gpioBase = NULL,
                .digitalFilter = false, }
        }
    },
    {
        .rxd = {
            .port = PORTC_CLK,
            .pincfg = {
                .base = PORTC,
                .pinPortIdx = 6u,
                .pullConfig = PORT_INTERNAL_PULL_NOT_ENABLED,
                .passiveFilter = false,
                .driveSelect = PORT_LOW_DRIVE_STRENGTH,
                .mux = PORT_MUX_ALT3,
                .pinLock = false,
                .intConfig = PORT_DMA_INT_DISABLED,
                .clearIntFlag = false,
                .gpioBase = NULL,
                .digitalFilter = false, }
        },
        .txd = {
            .port = PORTC_CLK,
            .pincfg = {
                .base = PORTC,
                .pinPortIdx = 7u,
                .pullConfig = PORT_INTERNAL_PULL_NOT_ENABLED,
                .passiveFilter = false,
                .driveSelect = PORT_LOW_DRIVE_STRENGTH,
                .mux = PORT_MUX_ALT3,
                .pinLock = false,
                .intConfig = PORT_DMA_INT_DISABLED,
                .clearIntFlag = false,
                .gpioBase = NULL,
                .digitalFilter = false, }
        }
    },
    {
        .rxd = {
            .port = PORTC_CLK,
            .pincfg = {
                .base = PORTC,
                .pinPortIdx = 16u,
                .pullConfig = PORT_INTERNAL_PULL_NOT_ENABLED,
                .passiveFilter = false,
                .driveSelect = PORT_LOW_DRIVE_STRENGTH,
                .mux = PORT_MUX_ALT3,
                .pinLock = false,
                .intConfig = PORT_DMA_INT_DISABLED,
                .clearIntFlag = false,
                .gpioBase = NULL,
                .digitalFilter = false, }
        },
        .txd = {
            .port = PORTC_CLK,
            .pincfg = {
                .base = PORTC,
                .pinPortIdx = 17u,
                .pullConfig = PORT_INTERNAL_PULL_NOT_ENABLED,
                .passiveFilter = false,
                .driveSelect = PORT_LOW_DRIVE_STRENGTH,
                .mux = PORT_MUX_ALT3,
                .pinLock = false,
                .intConfig = PORT_DMA_INT_DISABLED,
                .clearIntFlag = false,
                .gpioBase = NULL,
                .digitalFilter = false, }
        }
    }
};

/*
 * End of file
 * --------------------------------------------------------------------------------------------------------------------
 */
