/* MIT License
 *
 * Copyright (c) [2020] [Zoltan Dolensky]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef CAN_ROUTER_H_
#define CAN_ROUTER_H_

#include "can_router_types.h"

/*!
 * @defgroup can_router CAN ROUTER
 * @ingroup can_router
 * @addtogroup can_router
 * @{
 */

#if defined(__cplusplus)
extern "C" {
#endif

/*
 * ! @brief Create can_router instance internal structure.
 *
 *   @param[in]   Router channel index.
 *   @return    STATUS_SUCCESS if successful;
 *              STATUS_ERROR if instance is already created;
 *              STATUS_UNSUPPORTED if channel index is out of range;
 */
status_t can_router_channel_create(can_router_channel_t channel);

/*
 * ! @brief Destroy can_router instance internal structure.
 *
 *   @param[in]   Router channel index.
 *   @return    STATUS_SUCCESS if successful;
 *              STATUS_BUSY if a resource is not ready to be destroyed;
 *              STATUS_UNSUPPORTED if channel index is out of range;
 */
status_t can_router_channel_destroy(can_router_channel_t channel);

/*
 * ! @brief Initialize FlexCAN peripheral and open can_router channel allowing bus access.
 *
 *   @param[in]   Router channel index.
 *   @return    STATUS_SUCCESS if successful;
 *              STATUS_ERROR if unsuccessful;
 *              STATUS_UNSUPPORTED if channel index is out of range;
 */
status_t can_router_channel_open(can_router_channel_t channel);

/*
 * ! @brief Close can_router channel and deinitialize FlexCAN peripheral.
 *
 *   @param[in]   Router channel index.
 *   @return    STATUS_SUCCESS if successful;
 *              STATUS_ERROR if unsuccessful;
 *              STATUS_UNSUPPORTED if channel index is out of range;
 */
status_t can_router_channel_close(can_router_channel_t channel);

/*
 * ! @brief Search for available mailbox in pool and return its index.
 *
 *   @note  If no free mailbox is found, value of provided pointer is left unchanged.
 *          Check return value for application safety improvement.
 *
 *   @param[in]   Router channel index.
 *   @param[out]  Index of free mailbox.
 *   @return    STATUS_SUCCESS if successful;
 *              STATUS_ERROR if unsuccessful;
 *              STATUS_BUSY if no available mailbox is found;
 *              STATUS_UNSUPPORTED if channel index is out of range;
 */
status_t can_router_get_free_mailbox(can_router_channel_t channel, uint8_t *mailbox);

/*
 * ! @brief Aquire lock on mailbox.
 *
 *   @param[in]   Router channel index.
 *   @param[in]   Index of mailbox to be aquired.
 *   @param[in]   Callback function for mailbox notifications.
 *   @param[in]   User defined variable to be passed as notification argument.
 *   @return    STATUS_SUCCESS if successful;
 *              STATUS_ERROR if unsuccessful;
 *              STATUS_UNSUPPORTED if channel index is out of range;
 */
status_t can_router_aquire_mailbox(can_router_channel_t channel, uint8_t mailbox, can_router_callback_t callback,
        void *context);

/*
 * ! @brief Release lock on mailbox.
 *
 *   @param[in]   Router channel index.
 *   @param[in]   Index of mailbox to be released.
 *   @return    STATUS_SUCCESS if successful;
 *              STATUS_ERROR if unsuccessful;
 *              STATUS_BUSY if a resource is busy;
 *              STATUS_UNSUPPORTED if channel index is out of range;
 */
status_t can_router_free_mailbox(can_router_channel_t channel, uint8_t mailbox);

/*
 * ! @brief Transmit message using can_router mailbox.
 *
 *   @param[in]   Router channel index.
 *   @param[in]   Index of mailbox to be released.
 *   @param[in]   Message to be transmitted.
 *   @return    STATUS_SUCCESS if successful;
 *              STATUS_BUSY if a resource is busy;
 *              STATUS_ERROR if unsuccessful;
 *              STATUS_CAN_BUFF_OUT_OF_RANGE if the mailbox index is out of range;
 *              STATUS_UNSUPPORTED if channel index is out of range;
 */
status_t can_router_transmit(can_router_channel_t channel, uint8_t mailbox, can_message_t *message);

/*
 * ! @brief Configure mailbox to automatically send response to remote frames.
 *
 *   @note  Currently not supported.
 *
 *   @todo  Implement.
 *
 *   @param[in]   Router channel index.
 *   @param[in]   Index of used mailbox.
 *   @param[in]   Message to be transmitted.
 *   @return    STATUS_UNSUPPORTED;
 */
status_t can_router_set_rtr(can_router_channel_t channel, uint8_t mailbox, can_message_t *message);

/*
 * ! @brief Receive message using can_router mailbox.
 *
 *   @param[in]   Router channel index.
 *   @param[in]   Index of used mailbox.
 *   @param[out]  Message received.
 *   @return    STATUS_SUCCESS if successful;
 *              STATUS_BUSY if a resource is busy;
 *              STATUS_ERROR if unsuccessful;
 *              STATUS_CAN_BUFF_OUT_OF_RANGE if the mailbox index is out of range;
 *              STATUS_UNSUPPORTED if channel index is out of range;
 */
status_t can_router_receive(can_router_channel_t channel, uint8_t mailbox, can_message_t *message);

#if defined(__cplusplus)
}
#endif

/*! @}*/

#endif /* CAN_ROUTER_H_ */
